{
	inputs = {
		# Should follow a consuming flake's.
		nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
		flake-parts.url = "github:hercules-ci/flake-parts";

		nvf = {
			url = "github:notashelf/nvf";
			inputs = {
				nixpkgs.follows = "nixpkgs";
				flake-parts.follows = "flake-parts";
			};
		};

		blink-cmp = {
			url = "github:saghen/blink.cmp";
			inputs = {
				nixpkgs.follows = "nixpkgs";
				flake-parts.follows = "flake-parts";
			};
		};

		# neovim-nightly = {
		# 	url = "github:nix-community/neovim-nightly-overlay";
		# 	inputs.nixpkgs.follows = "nixpkgs";
		# };
	};

	outputs = { flake-parts, ... } @ inputs: flake-parts.lib.mkFlake { inherit inputs; } {
		debug = true;
		systems = [ "x86_64-linux" ];

		perSystem = { pkgs, ... }: {
			packages = rec {
				default = (inputs.nvf.lib.neovimConfiguration {
					inherit pkgs;
					extraSpecialArgs = inputs;
					modules = [ (import ./default.nix) ];
				}).neovim;
				neovim = default;
			};
		};

		flake = {
			# homeManagerModules = rec {
			# 	default = nvim;
			# 	# pkgs binding is manditory! HM checks params before injecting pkgs.
			# 	nvim = {pkgs, ...} @ f: (import ./default.nix) ({
			# 		# inherit neovim-nightly;
			# 	}//f);
			# };
		};
	};
}
