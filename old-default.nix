{ pkgs, lib, config, ... }: let
	# neovim-nightly-pkg = neovim-nightly.packages.${pkgs.system}.neovim;

	inherit (pkgs.toph-utils) mkEnabledOption;

	parrentCfg = config.custom-toph.cli;
	cfg = parrentCfg.nvim;

	mkIf = fi: lib.mkIf (parrentCfg.enable && fi);
in {
	options.custom-toph.cli.nvim = { enable = mkEnabledOption "Neovim module"; };

	config = {
		home.file = mkIf cfg.enable {
			# Really need a better way to manage this.
			# ".config/nvim/lazy-lock.json".source = config.lib.file.mkOutOfStoreSymlink (./lazy-lock.json);
			".config/nvim/lua".source = ./lua;
			".config/nvim/init.lua".source = ./init.lua;
		};
		home.packages = (mkIf cfg.enable) (with pkgs; [
			neovim
			# neovim-nightly-pkg
			nodejs
		]);
	};
}

