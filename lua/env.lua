-- Set vim vars

vim.o.nu = true -- enable gutter numbers
vim.o.rnu = true -- enable relative numbers
vim.opt.signcolumn = "yes:2"  -- column used by TODO and lsp signs. sets to always on, with 2 char width.

vim.opt.updatetime = 400  -- (ms) delay before recovery file is written, and CursorHold autocommand is triggered.

vim.o.cul = true	-- highlight cursor line
vim.o.ts = 4		-- Hard tab space
vim.o.sw = 4		-- Soft tab space
vim.o.sts = 4
vim.o.et = false	-- Don't expand tabs
vim.o.wrap = false	-- Don't line wrap

vim.o.ic = true		-- Ignore case
vim.o.scs = true	-- Smart case

vim.opt.listchars:append("tab:  →") -- \u{2192}
vim.opt.listchars:append("trail:◇") -- \u{25C7}
-- vim.opt.listchars:append("lead:\u{25c7}")
vim.opt.listchars:append("eol:§")
vim.opt.list = true

vim.o.wmnu = true
vim.o.wig = vim.o.wig .. "*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx"
vim.o.wig = vim.o.wig .. "*/node_modules/*"

-- vim.o.mouse = "a"

vim.o.foldmethod = "expr"
vim.o.foldexpr = "nvim_treesitter#foldexpr()"

vim.api.nvim_create_augroup("remember_folds", { clear = true })
vim.api.nvim_create_autocmd("BufWinEnter", {
	group = "remember_folds",
	pattern = "?*",
	command = "silent! loadview",
})
vim.api.nvim_create_autocmd("BufWinLeave", {
	group = "remember_folds",
	pattern = "?*",
	command = "mkview",
})

vim.g.rustfmt_autosave = 1

vim.g.rust_recommended_style = 0
