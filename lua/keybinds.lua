vim.g.mapleader = " "

local M = {
	set = function(mode, lhs, rhs, extras)
		if (lhs == nil) and (rhs == nil) and (extras == nil) then
			lhs = mode[2]
			rhs = mode[3]
			extras = mode[4]
			mode = mode[1]
		end
		if type(extras) == "string" then extras = { desc = extras } end
		if type(mode) == "string" then mode = vim.split(mode, "") end
		vim.keymap.set(mode, lhs, rhs, extras)
	end,
	auto_keys = {
		bufferline = function(mod)
			mod.setup() -- not sure why this is needed, but for whatever reason, this is needed for bufferline to show on start.
			return {
				{ "n", "<C-Tab>", function() mod.cycle(1) end },
				{ "n", "<C-S-Tab>", function() mod.cycle(-1) end },
			-- vimp.noremap("gx", "<CMD>bdelete<CR>")
			}
		end,
		["telescope.builtin"] = function(mod) return {
			{ "n", "<Leader><Tab>", mod.buffers },
			{ "n", "gff", mod.find_files },
			{ "n", "gts", mod.lsp_document_symbols },
			{ "n", "gtS", mod.lsp_dynamic_workspace_symbols },
			{ "n", "gfg", mod.live_grep },
		} end,
		spider = function (mod) -- remaps w, e, b, and ge motions to be sensitive to programming casing.
			return vim.iter({"w", "e", "b", "ge"})
				:map(function (key) return {"nox", key, function() mod.motion(key) end, "Spider-"..key } end)
				:totable()
		end,
		["neo-tree"] = { { "nox", "<Leader>e", [[:Neotree toggle<cr>]]} },
		vim = {
			-- Split Navigation
			{ "n", "<C-h>", "<C-w>h" },
			{ "n", "<C-j>", "<C-w>j" },
			{ "n", "<C-k>", "<C-w>k" },
			{ "n", "<C-l>", "<C-w>l" },

			{ "n", "<C-S-h>", "<C-w>>" },
			{ "n", "<C-S-j>", "<C-w>-" },
			{ "n", "<C-S-k>", "<C-w>+" },
			{ "n", "<C-S-l>", "<C-w><" },
			{ "nox", "<Leader>/", function() vim.o.hls = not vim.o.hls end, "Toggle highlight search" },

			-- Help navigation
			{ "n", "<CR>", "<C-j>" },

			-- swap mark jumps. grave key is too far away, but better than quote.
			{ "nox", "`", "'" },
			{ "nox", "'", "`" },

			{ "i", "<C-G>h", vim.lsp.buf.hover },
			not vim.g.vscode and {"n", "gh", vim.lsp.buf.hover} or nil,

			-- Insert escapes
			{ "i", "jj", "<esc>" },
			{ "i", "<C-c>", "<esc>" },
		}
	},
	cmp = function()
		local cmp = require("cmp")
		local cmp_actions = require("lsp-zero").cmp_action()

		return cmp.mapping.preset.insert {
			["<CR>"] = cmp.mapping.confirm { select = false },

			["<C-u>"] = cmp.mapping.scroll_docs(-4),
			["<C-d>"] = cmp.mapping.scroll_docs(4),

			["<Tab>"] = cmp_actions.luasnip_supertab(),
			["<S-Tab>"] = cmp_actions.luasnip_shift_supertab(),
		}
	end,
}

M.init = function(mod_name)
	return function ()
		local maps = M.auto_keys[mod_name]
		if type(maps) == "function" then
			local mod_ok, mod = pcall(require, mod_name)
			if not mod_ok then return print(("%s failed to load"):format(mod_name)) end
			maps = maps(mod)
		end

		vim.iter(maps)
			:filter(function (x) return x ~= nil end)
			:each(function(...) return M.set(...) end)
	end
end

M.init("vim")()

return M
