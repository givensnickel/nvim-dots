return {
	-- { "akinsho/flutter-tools.nvim", config = true },
	{ "neovim/nvim-lspconfig",
		dependencies = {
			"williamboman/mason-lspconfig.nvim",
			"williamboman/mason.nvim",
			{ "mrcjkb/rustaceanvim", lazy = false },
			{ "nvimtools/none-ls.nvim" },
			{ "VonHeikemen/lsp-zero.nvim", branch = "v4.x" },
		},
		config = function ()
			local lsp_zero = require "lsp-zero"
			lsp_zero.extend_lspconfig()

			lsp_zero.on_attach(function (_lsp_client, buffer)
				pcall(vim.lsp.inlay_hint.enable, buffer, true)

				local bufmap = function(mode, lhs, rhs, desc)
					return vim.keymap.set(mode, lhs, rhs, {buffer = true, desc = desc})
				end

				lsp_zero.default_keymaps { buffer = buffer, exclude = { "<F2>" }, }
				bufmap('n', '<leader>r', vim.lsp.buf.rename, "Renames all references")
			end)

			if vim.loop.os_uname().version:find("NixOS") then
				require("mason-registry"):on("package:install:success", function(pkg)
					pkg:get_receipt():if_present(function(receipt)
						-- Figure out the interpreter inspecting nvim itself
						-- This is the same for all packages, so compute only once
						local interpreter = io.popen("patchelf --print-interpreter `which nvim`"):read("*a"):gsub("%s+", "")

						for _, rel_path in pairs(receipt.links.bin) do
							local bin_abs_path = pkg:get_install_path() .. "/" .. rel_path
							if pkg.name == "lua-language-server" then
								bin_abs_path = pkg:get_install_path() .. "/libexec/bin/lua-language-server"
							end

							-- Set the interpreter on the binary
							os.execute(("patchelf --set-interpreter %q %q"):format(interpreter, bin_abs_path))
						end
					end)
				end)
			end

			require("mason").setup()
			local mason_lsp = require("mason-lspconfig")
			mason_lsp.setup {
				handlers = {
					lsp_zero.default_setup,
					["rust_analyzer"] = function ()  end,
				},
				automatic_installation = true,
				ensure_installed = { }
			}

			for _, server in iparis(mason_lsp.get_installed_servers()) do
				lspconfig
			end
		end
	},

	-- Completion engine
	{ "hrsh7th/nvim-cmp",
		dependencies = {
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/vim-vsnip",
			"L3MON4D3/LuaSnip",
			{ "VonHeikemen/lsp-zero.nvim", branch = "v3.x" },
		},
		config = function ()
			vim.opt.completeopt = { "menu", "menuone", "noselect" }

			local cmp = require("cmp")
			local luasnip = require("luasnip")

			cmp.setup {
				mapping = require("keybinds").cmp(),
				snippet = { expand = function(args) luasnip.lsp_expand(args.body) end },
				sources = {
					{ name = "path" },
					{ name = "nvim_lsp", keyword_lenght = 1 },
					{ name = "buffer", keyword_lenght = 2 },
					{ name = "luasnip", keyword_lenght = 3 },
				},
				window = {
					documentation = cmp.config.window.bordered(),
					completion = cmp.config.window.bordered(),
				},
				formatting = {
					expandable_indicator = true,
					fields = { "menu", "abbr", "kind" },
					format = function(entry, item)
						local menu_icons = {
							nvim_lsp = "λ",
							luasnip = "⋗",
							buffer = "Ω",
							path = ""
						}

						item.menu = menu_icons[entry.source.name]
						return item
					end
				},
			}
		end
	},

	{ "folke/trouble.nvim", config = true, },

}
