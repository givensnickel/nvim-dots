-- Diagnostics
local signs = { text = {}, linehl = {}, numhl = {} }

for type, icon in pairs({ Error = "󰅚 ", Warn = " ", Hint = "󰌶 ", Info = " " }) do
	local hl = "DiagnosticSign" .. type

	signs.text[vim.diagnostic.severity[type:upper()]] = icon
	signs.linehl[vim.diagnostic.severity[type:upper()]] = hl
	signs.numhl[vim.diagnostic.severity[type:upper()]] = hl
end

vim.diagnostic.config {
	virtual_text = true,
	signs = signs,
	underline = true,
	update_in_insert = true,
	severity_sort = false,
	float = {
		border = "rounded",
		source = true
	}
}

return {
	"tpope/vim-sleuth",	-- detect indent
	{ "karb94/neoscroll.nvim", config = true },
	{ "stevearc/dressing.nvim", config = true },
	{ "kyazdani42/nvim-web-devicons", lazy = true },
	{ "nvim-lualine/lualine.nvim",
		event = "VeryLazy",
		config = require("lualine-config")
	}, -- Status line plugin

	{ "j-hui/fidget.nvim", -- corner notifications
		opts = { window = { blend = 0 } },
		tag = "legacy",
		event = "LspAttach"
	},
	{ "srcery-colors/srcery-vim",
		lazy = false,
		priority = 1000,
		config = function()
			-- Apply theme
			vim.cmd("colo srcery")

			-- Make background transparent
			vim.cmd("hi Normal guibg=NONE ctermbg=NONE")
			vim.cmd("hi Folded guibg=NONE ctermbg=NONE")

			vim.cmd("hi SignColumn guibg=NONE ctermbg=NONE")
			vim.cmd("hi link CursorLineSign CursorLineNr")

			vim.cmd("hi link @lsp.type.class @constructor")
			vim.cmd("hi link @include.typescript Keyword")
		end
	},
	{ "nvim-treesitter/nvim-treesitter",
		event = "VeryLazy",
		dependencies = { "nvim-treesitter/nvim-treesitter-context" },
		config = function ()
			local treepath = vim.fn.stdpath("data") .. "/treesitter"
			vim.opt.runtimepath:append(treepath)

			---@diagnostic disable-next-line missing-fields
			require 'nvim-treesitter.configs'.setup {
				parser_install_dir = treepath,
				ensure_installed = {"rust", "lua", "python", "typescript", "dart"},
				additional_nvim_regex_higlighting = false,
				highlight = {
					enable = true
				},
				indent = {
					enable = true,
				},
			}
		end
	},
	{ "HiPhish/rainbow-delimiters.nvim",
		event = "VeryLazy",
		config = function()
			local rb_delim = require "rainbow-delimiters"
			require "rainbow-delimiters.setup".setup {
				strategy = { [""] = rb_delim.strategy["global"] },
				query = { [""] = "rainbow-delimiters" },
				highlight = {
					"RainbowDelimiterRed",
					"RainbowDelimiterYellow",
					"RainbowDelimiterBlue",
					"RainbowDelimiterOrange",
					"RainbowDelimiterGreen",
					"RainbowDelimiterViolet",
					"RainbowDelimiterCyan",
				}
			}
		end
	},
	{ "lukas-reineke/indent-blankline.nvim",
		event = "VeryLazy",
		main = "ibl",
		config = function ()
			local highlight = {
				"RainbowRed",
				"RainbowYellow",
				"RainbowBlue",
				"RainbowOrange",
				"RainbowGreen",
				"RainbowViolet",
			}

			local colors = {
				"#ef2f27",
				"#519f50",
				"#fbb829",
				"#2c78bf",
				"#e02c6d",
				"#0aaeb3"
			}

			local hooks = require("ibl.hooks")
			hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
				for i = 1, #highlight do
					vim.api.nvim_set_hl(0, highlight[i], { fg = colors[i], nocombine = true })
				end
			end)

			-- vim.g.rainbow_delimiters = { highlight = highlight }
			require("ibl").setup {
				indent = {
					char="╏",
					highlight = "IblIndent",
				},
				scope = {
					char="║",
					show_start = true,
					highlight = highlight,
					show_end = true,
					priority = 2,
				}
			}
		end
	},
}
