local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.uv.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

local is_work, work = pcall(require, "work")

require "env"
local keys = require "keybinds"

local pretty = require("pretty")
local lang_features = require("lang_features")

local init_mods = {
	-- Libs -- should be removed.
	"nvim-lua/plenary.nvim", -- Needed for (Telescope, rust-tools, vgit)

	-- Navigation
	{ "nvim-neo-tree/neo-tree.nvim", -- DONE
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
			"MunifTanjim/nui.nvim",
		},
		config = function ()
			require("neo-tree").setup {
				close_if_last_window = true;
				window = { position = "right", },
			}
			keys.init("neo-tree")()
		end
	},
	{ "nvim-telescope/telescope.nvim", config = keys.init "telescope.builtin" },
	"simrat39/symbols-outline.nvim",
	{ "chrisgrieser/nvim-spider", config = keys.init "spider" },
	{ "folke/todo-comments.nvim", opts = { merge_keywords = true } },
	"famiu/bufdelete.nvim",
	{ "akinsho/bufferline.nvim",
		event = "VeryLazy",
		config = keys.init "bufferline"
	},
	-- UFO?

	-- Tinker
	"rafcamlet/nvim-luapad",
	{ "folke/lazydev.nvim", config = true },
	{ "akinsho/toggleterm.nvim",
		opts = {
			open_mapping = [[<c-\>]],
			direction = "float",		-- Might change to tab or horizontal
			float_opts = { border = "curved" },
		}
	},

	-- Other
	"tpope/vim-fugitive", -- git
	"tpope/vim-rhubarb",  -- hub
	{ "numToStr/Comment.nvim", config = true },
	{ "windwp/nvim-autopairs", config = true },
	{ "lewis6991/gitsigns.nvim",
		opts = {
			-- See `:help gitsigns.txt`
			signs = {
				add = { text = '+' },
				change = { text = '~' },
				delete = { text = '_' },
				topdelete = { text = '‾' },
				changedelete = { text = '~' },
			},
			on_attach = function(bufnr)
				vim.keymap.set('n', '<leader>hp', require('gitsigns').preview_hunk, { buffer = bufnr, desc = 'Preview git hunk' })

				-- don't override the built-in and fugitive keymaps
				local gs = package.loaded.gitsigns
				vim.keymap.set({'n', 'v'}, ']c', function()
					if vim.wo.diff then return ']c' end
					vim.schedule(function() gs.next_hunk() end)
					return '<Ignore>'
				end, {expr=true, buffer = bufnr, desc = "Jump to next hunk"})
				vim.keymap.set({'n', 'v'}, '[c', function()
					if vim.wo.diff then return '[c' end
					vim.schedule(function() gs.prev_hunk() end)
					return '<Ignore>'
				end, {expr=true, buffer = bufnr, desc = "Jump to previous hunk"})
			end,
		},
	},
	{ "norcalli/nvim-colorizer.lua",
		opts = {"*"},
		init = function() vim.o.termguicolors = true end,
	},
	{ "folke/which-key.nvim",
		event = "VeryLazy",
		init = function()
			vim.o.timeout = true
			vim.o.timeoutlen = 300
		end,
		config = true
	},

}

vim.list_extend(init_mods, pretty)
vim.list_extend(init_mods, lang_features)
if is_work then vim.list_extend(init_mods, work) end

require("lazy").setup(init_mods)
