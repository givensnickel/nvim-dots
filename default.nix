# Failed to run 'after' hook for blink-cmp:
# ...ir/pack/mnw/opt/blink-cmp/lua/blink/cmp/config/utils.lua:33:
# signature.enable: unexpected field found in configuration

{ pkgs, ... } @ inputs: let
	inherit (builtins) listToAttrs map;
	quickEnable = langs: listToAttrs (map (lang: { name = lang; value = { enable = true; }; }) langs);
	blink-cmp = inputs.blink-cmp.packages.${pkgs.system}.blink-cmp;
in {
	vim = {
		extraPackages = with pkgs; [ fzf ripgrep ];
		pluginOverrides = { inherit blink-cmp; };
		lazy.plugins = with pkgs.vimPlugins; {
			srcery-vim = {
				package = srcery-vim;

				after = /* lua */ ''
					vim.cmd('colorscheme srcery')

					-- Make background transparent
					vim.cmd("hi Normal guibg=NONE ctermbg=NONE")
					vim.cmd("hi Folded guibg=NONE ctermbg=NONE")

					vim.cmd("hi SignColumn guibg=NONE ctermbg=NONE")
					vim.cmd("hi link CursorLineSign CursorLineNr")

					vim.cmd("hi link @lsp.type.class @constructor")
					vim.cmd("hi link @include.typescript Keyword")
				'';
			};
			vim-sleuth.package = vim-sleuth; # Auto detect indent
			"snacks.nvim" = {
				package = snacks-nvim;

				setupModule = "snacks";
				setupOpts = {
					notifier.enable = true;
					input.enable = true;
					bigfile.enable = false; # Might be interesting to enable. Prevents LSP/TS on large files.
				};
			};
		};

		options = {
			number = true;
			relativenumber = true;

			tabstop = 4;
			softtabstop = 4;
			shiftwidth = 4;
			expandtab = false;

			wrap = false;
			signcolumn = "yes:2";

			updatetime = 400;

			ic = true;
			scs = true;

			# listcars += [ "tab:  " "trail:" "eol:" ]
			# list = true;

			# wmnu -- wildmenu
			# wig

			foldmethod = "expr";
			foldexpr = "nvim_treesitter#foldexpr()";


			# vim.api.nvim_create_augroup("remember_folds", { clear = true })
			# vim.api.nvim_create_autocmd("BufWinEnter", {
			# 	group = "remember_folds",
			# 	pattern = "?*",
			# 	command = "silent! loadview",
			# })
			# vim.api.nvim_create_autocmd("BufWinLeave", {
			# 	group = "remember_folds",
			# 	pattern = "?*",
			# 	command = "mkview",
			# })
		};

		globals.mapleader = " ";

		keymaps = [
			# -- Insert escapes
			{ mode = "i"; key = "jj"; action = "<esc>"; }

			{ mode = "n"; key = "<C-h>"; action = "<C-w>h"; }
			{ mode = "n"; key = "<C-j>"; action = "<C-w>j"; }
			{ mode = "n"; key = "<C-k>"; action = "<C-w>k"; }
			{ mode = "n"; key = "<C-l>"; action = "<C-w>l"; }

			# -- swap mark jumps. grave key is too far away, but better than quote.
			{ mode = "nox"; key = "`"; action = "'"; }
			{ mode = "nox"; key = "'"; action = "`"; }

			# { "n", "<C-S-h>", "<C-w>>" },
			# { "n", "<C-S-j>", "<C-w>-" },
			# { "n", "<C-S-k>", "<C-w>+" },
			# { "n", "<C-S-l>", "<C-w><" },
			# { "nox", "<Leader>/", function() vim.o.hls = not vim.o.hls end, "Toggle highlight search" },
			#
			# -- Help navigation
			# { "n", "<CR>", "<C-j>" },
			#
			# { "i", "<C-G>h", vim.lsp.buf.hover },
			# not vim.g.vscode and {"n", "gh", vim.lsp.buf.hover} or nil,
		];

		treesitter = {
			enable = true;
			context.enable = true;
		};

		spellcheck.programmingWordlist.enable = true;

		autocomplete = {
			blink-cmp = {
				enable = true;
				setupOpts = {
					# signature.enable = true; # Currently not working, not sure why.
					cmdline.sources = null;
				};
			};
		};

		lsp = {
			formatOnSave = true;

			# Adds pictograms to lsp, like the module/function/struct/const distinguisher.
			lspkind.enable = false; # omitted in favor of blink.cmp options

			# This adds a lightbulb virt text to lines with code actions
			lightbulb.enable = true;

			# In depth LSP ui.
			lspsaga.enable = false;

			trouble.enable = true;

			# Lsp hinting on function signatures
			lspSignature.enable = false; # blink opt prefered

			# Supports lsp of embedded languages.
			otter-nvim.enable = true;

			# Enables virt text lsp diagnostics.
			lsplines.enable = true;

			# Under window docs reader for hover.
			nvim-docs-view.enable = false;
		};

		languages = {
			enableLSP = true;
			enableFormat = true;
			enableTreesitter = true;

			# Not totally sure what this is... Usually seems to add null-ls if provided in modules.
			enableExtraDiagnostics = true;

			nix = {
				enable = true;
				format.enable = true;
			};
			rust = {
				enable = true;
				crates.enable = true;
			};
			lua = {
				enable = true;
				lsp.lazydev.enable = true;
			};
		} // quickEnable [
				"bash"
				"clang"
				"css"
				"elixir"
				"html"
				"java"
				"python"
				"sql"
				"ts"
				"zig"
		];

		telescope = {
			enable = true;
			mappings = {
				buffers = "<leader><tab>";
				findFiles = "gff";
				lspDocumentSymbols = "gts";
				# lspDynamicWorkspaceSymbols = "gtS";
				liveGrep = "gfg";
			};
		};

		visuals = {
			nvim-scrollbar.enable = true;
			nvim-web-devicons.enable = true;
			nvim-cursorline.enable = true; # highlights word under cursor
			highlight-undo.enable = true; # highlights undo/redo and expanded to things like substitute.
			indent-blankline.enable = true;
			rainbow-delimiters.enable = true;
		};

		statusline = {
			lualine = {
				enable = true;
			};
		};

		tabline = {
			nvimBufferline = {
				enable = true;
				setupOpts.options.numbers = "none";
				mappings = {
					cycleNext = "<C-Tab>";
					cyclePrevious = "<C-S-Tab>";
				};
			};
		};

		mini = { } // quickEnable [ "animate" "comment" "diff" "move" "pairs" ];

		autopairs.nvim-autopairs.enable = false; # omitted in preference to mini.pairs

		snippets.luasnip.enable = true;

		filetree.neo-tree = {
			enable = true;
			setupOpts = {
				git_status_async = true;
				close_if_last_window = true;
				window.position = "right";
			};
		};

		git = {
			enable = true;
			gitsigns.enable = true;
			gitsigns.codeActions.enable = false;
		};

		minimap.codewindow.enable = true;

		utility = {
			images.image-nvim = {
				enable = true;
				setupOpts = {
					backend = "kitty";
				};
			};
			# ccc.enable = true; # color picker; Currently conflicts with otter.
		};

		binds = {
			whichKey.enable = true;
			cheatsheet.enable = true;
		};

		notes = {
			obsidian.enable = false; # https://github.com/epwalsh/obsidian.nvim
			neorg.enable = false; # https://github.com/nvim-neorg/neorg
			todo-comments = {
				enable = true;
				setupOpts.merge_keywords = true;
			};
		};

		ui = {
			borders.enable = true;
			noice.enable = true; # pretty cmdline
			colorizer.enable = true; # May conflict with ccc?
			illuminate.enable = true; # highlights same tokens
			breadcrumbs = {
				enable = true;
				navbuddy.enable = true;
			};
			smartcolumn = {
				enable = false;

				setupOpts.custom_colorcolumn = {
					# lang = int
				};
			};
			fastaction.enable = true; # Ergonomic code actions ui.
		};
	};
}
